#!/bin/bash
find . -name .hg -prune -o -name comm -prune -o -name '*.js' -exec egrep 'do_check_eq\(|do_check_neq\(|do_check_true\(|do_check_false\('  {} \; -a -print | grep '^\./' >/tmp/files$$
for i in `cat /tmp/files$$` ; do  echo $i; cp $i $i.original; sed -e 's@todo_check_@doxyzzycheck_@g' -e 's@_do_check_@UdoUcheckU@g' -e 's@do_check_eq[(]@Assert.equal(@g' -e 's@do_check_neq[(]@Assert.notEqual(@g' -e 's@do_check_true[(]@Assert.ok(@g' -e 's@do_check_false[(]@Assert.ok(!@g' -e 's@UdoUcheckU@_do_check_@g' -e 's@doxyzzycheck_@todo_check_@g' < $i.original > $i; rm $i.original; done
rm -f /tmp/files$$
