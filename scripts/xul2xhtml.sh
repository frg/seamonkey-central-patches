#!/bin/bash
GIT_REPO=https://gitlab.com/frg/seamonkey-central-patches.git
# Uncomment the following line to ignore the GIT patch queue when
# creating the patches.
#GIT_REPO=
ERROR=0
rm -rf /tmp/comm-xul$$ /tmp/git-xul$$
pushd /tmp
hg clone https://hg.mozilla.org/comm-central/ /tmp/comm-xul$$ || ERROR=1 
if [ $ERROR -eq 1 ]
then
  rm -rf /tmp/*-xul$$
  echo "failed to clone comm-central"
  exit 1
fi
if [ "$GIT_REPO" != "" ]
then
  mkdir /tmp/git-xul$$
  cd /tmp/git-xul$$
  git clone $GIT_REPO || ERROR=1
  if [ $ERROR -eq 1 ]
  then
    rm -rf /tmp/*-xul$$
    echo "failed to clone git patch queue"
    exit 1
  fi
  cd /tmp/comm-xul$$
  if [ -s /tmp/git-xul$$/seamonkey-central-patches/comm-central/patches/series ]
  then
    for i in `cat /tmp/git-xul$$/seamonkey-central-patches/comm-central/patches/series`
    do
      if [ "${i:0:12}" = "TOP-1611647-" ]
      then
        break
      fi
       hg import /tmp/git-xul$$/seamonkey-central-patches/comm-central/patches/$i || ERROR=1
      if [ $ERROR -eq 1 ]
      then
        rm -rf /tmp/*xul$$
        echo "failure applying $i"
        exit 1
      fi
    done
  fi
fi
cd /tmp/comm-xul$$
find suite -name '*.xul' -print > /tmp/filelist$$
for i in `sed -e 's@\.xul$@@' /tmp/filelist$$`
do
  hg mv $i.xul $i.xhtml
done
hg commit -q -m "Bug 1611647 - Port bug 1579952 - Mass rename .xul files to .xhtml in suite."
CHANGESET_1=`hg history -l 1 | grep changeset | sed -e 's@changeset:[ ]*@@'`
find suite -type f -exec grep '\.xul\b' {} \; -a -print > /tmp/filelist$$
for i in `grep '^suite' /tmp/filelist$$`
do
  sed -e 's@\.xul   @.xhtml @g' -e 's@\.xul\.   @.xhtml. @g' -e 's@\.xul\b@.xhtml@g' < $i > $i.$$
  sed -e 's@there\.is\.only\.xhtml@there.is.only.xul@' -e 's@vnd\.mozilla\.xhtml+xml@vnd.mozilla.xul+xml@' < $i.$$ > $i
  rm $i.$$
done
rm /tmp/filelist$$
hg commit -q -m "Bug 1611647 - Port bug 1579952 - Fix references to xul files in suite."
CHANGESET_2=`hg history -l 1 | grep changeset | sed -e 's@changeset:[ ]*@@'`
popd
hg export -R /tmp/comm-xul$$ $CHANGESET_1 > TOP-1611647-1-rename-xul-suite.patch
hg export -R /tmp/comm-xul$$ $CHANGESET_2 > TOP-1611647-2-fix-xul-references-suite.patch
rm -rf /tmp/comm-xul$$ /tmp/git-xul$$
