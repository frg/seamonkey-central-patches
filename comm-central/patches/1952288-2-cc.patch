# HG changeset patch
# User Matt A. Tobin <email@mattatobin.com>
# Date 1741281266 0
Bug 1952288 - Part 2: Revamp suite/moz.configure. r=IanN DONTBUILD

This cleans up moz.configure into logical blocks and loops through simple
key/value options and remaining defines.

diff --git a/suite/moz.configure b/suite/moz.configure
--- a/suite/moz.configure
+++ b/suite/moz.configure
@@ -1,176 +1,164 @@
 # -*- Mode: python; c-basic-offset: 4; indent-tabs-mode: nil; tab-width: 40 -*-
 # vim: set filetype=python:
 # This Source Code Form is subject to the terms of the Mozilla Public
 # License, v. 2.0. If a copy of the MPL was not distributed with this
 # file, You can obtain one at http://mozilla.org/MPL/2.0/.
 
-set_config("MOZ_SUITE", True)
-set_define("MOZ_SUITE", True)
-
-imply_option("MOZ_APP_NAME", "seamonkey")
-imply_option("MOZ_APP_BASENAME", "SeaMonkey")
-
-imply_option("MOZ_APP_ID", "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}")
-imply_option("MOZ_APP_VENDOR", "Mozilla")
+# =============================================================================
+# = Configuration and Options
+# =============================================================================
+@template
+def seamonkey_confvars():
+    config = {
+        "MOZ_SUITE": True,
+        "SEAMONKEY_VERSION": milestone.app_version,
+        "SEAMONKEY_VERSION_DISPLAY": milestone.app_version_display,
+    }
 
-imply_option("--enable-updater", True)
-imply_option("MOZ_PROFILE_MIGRATOR", True)
-# Include the DevTools client, not just the server (which is the default)
-imply_option("MOZ_DEVTOOLS", "all")
-imply_option("NSS_EXTRA_SYMBOLS_FILE", "../comm/mailnews/nss-extra.symbols")
-
-imply_option('--enable-default-browser-agent', False)
+    options = {
+        "--enable-crashreporter": True,             # MOZ_CRASHREPORTER
+        "--enable-updater": True,                   # MOZ_UPDATER
+        "BROWSER_CHROME_URL": "chrome://navigator/content/navigator.xul",
+        "MOZ_APP_BASENAME": "SeaMonkey",
+        "MOZ_APP_ID": "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}",
+        "MOZ_APP_VENDOR": "Mozilla",
+        "MOZ_DEVTOOLS": "all",
+        "MOZ_PLACES": True,
+        "MOZ_PROFILE_MIGRATOR": True,
+        "MOZ_SERVICES_SYNC": True,
+        "NSS_EXTRA_SYMBOLS_FILE": "../comm/mailnews/nss-extra.symbols",
+    }
 
-@depends(target_is_windows, target_has_linux_kernel)
-def bundled_fonts(is_windows, is_linux):
-    if is_windows or is_linux:
-        return True
+    # Config and Define
+    for k, v in config.items():
+        set_config(k, v)
+        set_define(k, v)
+        add_old_configure_assignment(k, v)
 
+    # Imply Options
+    for k, v in options.items():
+        imply_option(k, v)
 
-set_config("MOZ_BUNDLED_FONTS", bundled_fonts)
-add_old_configure_assignment("MOZ_BUNDLED_FONTS", bundled_fonts)
+# Do it.
+seamonkey_confvars()
 
-
+# =============================================================================
+# = Set Comm"s TOPSRCDIR
+# =============================================================================
 @depends(build_environment, "--help")
 @imports(_from="os.path", _import="join")
 def commtopsrcdir(build_env, _):
     topsrcdir = build_env.topsrcdir
     return join(topsrcdir, "comm")
 
+set_config("commtopsrcdir", commtopsrcdir)
+add_old_configure_assignment("commtopsrcdir", commtopsrcdir)
 
-@template
-def set_defconf(k, v):
-    set_config(k, v)
-    set_define(k, v)
-    add_old_configure_assignment(k, v)
+# =============================================================================
+# = Bundled Fonts
+# =============================================================================
+@depends(target_is_windows, target_has_linux_kernel)
+def bundled_fonts(is_windows, is_linux):
+    if is_windows or is_linux:
+        return True
 
+set_config("MOZ_BUNDLED_FONTS", bundled_fonts)
+add_old_configure_assignment("MOZ_BUNDLED_FONTS", bundled_fonts)
 
-set_define("MAR_CHANNEL_ID", "\"seamonkey-comm-release\"")
-add_old_configure_assignment("MAR_CHANNEL_ID", "seamonkey-comm-release")
+# =============================================================================
+# = Updater
+# =============================================================================
+set_define("MAR_CHANNEL_ID",
+           "\"seamonkey-comm-release\"")
+
+add_old_configure_assignment("MAR_CHANNEL_ID",
+                             "seamonkey-comm-release")
 
 set_define("ACCEPTED_MAR_CHANNEL_IDS",
-            "\"seamonkey-comm-release,seamonkey-comm-central\"")
+           "\"seamonkey-comm-release,seamonkey-comm-central\"")
+
 add_old_configure_assignment("ACCEPTED_MAR_CHANNEL_IDS",
                              "seamonkey-comm-release,seamonkey-comm-central")
 
-add_old_configure_assignment("commtopsrcdir", commtopsrcdir)
-set_config("commtopsrcdir", commtopsrcdir)
+# =============================================================================
+# = ChatZilla extension
+# =============================================================================
+option("--enable-irc",
+       default=False,
+       help="Enable building of the ChatZilla IRC extension")
 
+@depends_if("--enable-irc")
+def irc(arg):
+    return True
+
+set_config("MOZ_IRC", irc)
+
+# =============================================================================
+# = DebugQA extension
+# =============================================================================
+option("--enable-debugqa",
+       default=False,
+       help="Enable building of the DebugQA extension")
 
-@depends(build_environment, application)
-@imports(_from="os.path", _import="exists")
-@imports(_from="__builtin__", _import="open")
-def seamonkey_version(build_env, app_path):
-    version_file = os.path.join(
-        build_env.topsrcdir, app_path[0], "config", "version.txt"
-    )
-    version_file_display = os.path.join(
-        build_env.topsrcdir, app_path[0], "config", "version_display.txt"
-    )
-    version_file_package = os.path.join(
-        build_env.topsrcdir, app_path[0], "config", "version_package.txt"
-    )
-    rv = []
-    for f in [version_file, version_file_display, version_file_package]:
-        if exists(f):
-            f_value = open(f).read().strip()
-        else:
-            f_value = "unknown"
-        rv.append(f_value)
+@depends_if("--enable-debugqa")
+def debugqa(arg):
+    return True
+
+set_config("MOZ_DEBUGQA", debugqa)
 
-    return namespace(version=rv[0],
-                     version_display=rv[1],
-                     version_package=rv[2])
-
+# =============================================================================
+# = PIE
+# =============================================================================
+with only_when(target_has_linux_kernel & compile_environment):
+    option(env="MOZ_NO_PIE_COMPAT", help="Enable non-PIE wrapper")
 
-set_defconf("SEAMONKEY_VERSION", seamonkey_version.version)
-set_defconf("SEAMONKEY_VERSION_DISPLAY", seamonkey_version.version_display)
-# Currently not set in suite comm-central
-# set_defconf("MOZ_PKG_VERSION", seamonkey_version.version_package)
-imply_option("BROWSER_CHROME_URL", "chrome://navigator/content/navigator.xul")
+    set_config("MOZ_NO_PIE_COMPAT",
+               depends_if("MOZ_NO_PIE_COMPAT")(lambda _: True))
 
-
-imply_option("MOZ_PLACES", True)
-imply_option("MOZ_SERVICES_SYNC", False)
-
-# Thunderbird Rust code is now the default
-option("--disable-thunderbird-rust", help="Enable Rust support within Thunderbird")
+# =============================================================================
+# = Thunderbird Rust
+# =============================================================================
+option("--disable-thunderbird-rust",
+       help="Enable Rust support within Thunderbird")
 
 set_config("MOZ_THUNDERBIRD_RUST", True, when="--enable-thunderbird-rust")
 set_define("MOZ_THUNDERBIRD_RUST", True, when="--enable-thunderbird-rust")
 set_config("MOZ_OVERRIDE_GKRUST", True, when="--enable-thunderbird-rust")
 
-
 @depends("--enable-thunderbird-rust")
 def moz_override_cargo_config(enable_rust):
     rust_override = "comm/rust/.cargo/config.toml.in"
     if enable_rust:
         log.info(f"Using {rust_override} for Rust code.")
         return rust_override
 
 
-set_config(
-    "MOZ_OVERRIDE_CARGO_CONFIG",
-    moz_override_cargo_config,
-    when="--enable-thunderbird-rust",
-)
-
-# Building extensions is disabled by default.
-
-# =========================================================
-# = ChatZilla extension
-# =========================================================
-option(
-    "--enable-irc", default=False, help="Enable building of the ChatZilla IRC extension"
-)
-
-
-@depends_if("--enable-irc")
-def irc(arg):
-    return True
-
-
-set_config("MOZ_IRC", irc)
+set_config("MOZ_OVERRIDE_CARGO_CONFIG",
+           moz_override_cargo_config,
+           when="--enable-thunderbird-rust")
 
-# =========================================================
-# = DebugQA extension
-# =========================================================
-option(
-    "--enable-debugqa", default=False, help="Enable building of the DebugQA extension"
-)
-
-
-@depends_if("--enable-debugqa")
-def debugqa(arg):
-    return True
-
-
-set_config("MOZ_DEBUGQA", debugqa)
-
-with only_when(target_has_linux_kernel & compile_environment):
-    option(env='MOZ_NO_PIE_COMPAT',
-           help='Enable non-PIE wrapper')
-
-    set_config('MOZ_NO_PIE_COMPAT',
-               depends_if('MOZ_NO_PIE_COMPAT')(lambda _: True))
-
-# Miscellaneous programs
-# ==============================================================
-
-check_prog("ZIP", ("zip",))
-
+# =============================================================================
+# = Glean
+# =============================================================================
 set_config(
     "MOZ_GLEAN_EXTRA_METRICS_FILES",
     [
         "comm/mail/metrics.yaml",
         "comm/mail/components/compose/metrics.yaml",
         "comm/calendar/metrics.yaml",
     ],
 )
-# set_config("MOZ_GLEAN_EXTRA_PINGS_FILES", ["comm/mail/pings.yaml"])
+
 set_config("MOZ_GLEAN_EXTRA_TAGS_FILES", ["comm/mail/tags.yaml"])
 
+# =============================================================================
+# = Miscellaneous programs
+# =============================================================================
+check_prog("ZIP", ("zip",))
+
+# =============================================================================
+# = Includes
+# =============================================================================
 include("../build/moz.configure/gecko_source.configure")
-
 include("../mailnews/moz.configure")
 include("../../toolkit/moz.configure")
